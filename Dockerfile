FROM cgr.dev/chainguard/node:latest-dev

RUN node --version

WORKDIR /npm

RUN git clone -b provenance https://github.com/npm/cli.git

USER root

RUN cd cli && npm install -g

ENTRYPOINT ["npm"]
